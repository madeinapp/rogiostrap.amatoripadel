
# Rogiostrap®
**Rogiostrap** is the latest front-end framework, built with npm, bower, grunt, twig, sass.
Created by [Giordano Zannoni](https://it.linkedin.com/in/giordanozannoni).


1. [Environment settings](#environment-settings)
2. [Pipeline installation](#pipeline-installation)
3. [Text editors and linters](#text-editors-and-linters)
4. [Development](#development)
5. [Shell Commands](#shell-commands)
6. [Grid system usage](#grid-system-usage)
7. [Funtions and methods](#funtions-and-methods)
8. [Components](#components)

## Environment settings
- Install [Node.js and npm](https://nodejs.org/en/)
- Install [Grunt](https://gruntjs.com/)


		npm install -g grunt-cli

- Install [Bower](https://bower.io/)

		npm install -g bower

- Install [Sass](http://sass-lang.com/)

		npm install -g sass

- **Windows only** - Install [Ruby](https://rubyinstaller.org/)

- Install [Compass](http://compass-style.org/)

		gem install compass


## Pipeline installation

- **Create** `<your folder>`
- **Move** into `<your folder>`
- **Clone** the **Starterkit** git [repository](https://bitbucket.org/triboomedia/rogiostrap.starter), or run this command:

		git clone git@bitbucket.org:triboomedia/rogiostrap.starter.git .

- Replace the GIT reference project:

		git remote set-url origin git@bitbucket.org:triboomedia/<your repo>.git

- Install the `node_modules/` running:

		npm install

**(check for new packages or updates every time you pull from the repository)**

- Modify `settings.json` with the references of your theme:

		{
			"settings": {
				"theme": "<theme-name>",
				"domain": "<site-url>",
				"test": "local",
				"urlBuild": "/app/themes/<theme-name>",
				"buildFolder": "assets"
			}
		}


## Text editors & linters
We recommend [Atom GUI](https://atom.io/) with the following linters, to insure legibility and maintainability in the future operations:

- [linter](https://atom.io/packages/linter)
- [linter-eslint](https://atom.io/packages/linter-eslint)
- [linter-stylelint](https://atom.io/packages/linter-stylelint)
- [linter-ui-default](https://atom.io/packages/linter-ui-default)


- **Now you are ready to develop!**

## Development
Run `grunt start` to compile the local development build; it will auto-open the browser at `localhost:2323` and enter in “watch mode” (every time you save a change the pipeline will re-compile).

### Setting variables

Open your customVariables.scss and update all your site references: breakpoint, color palette, siteWidth, fonts, columns, etc...

### Structure
Rogiostrap has a *horizontally centered* **wrapper of 996px** with `padding: 0 8px;` so the **available content width is 980px**.

There are three types of components and all of them are managed through [Twig](https://twig.symfony.com/).  

- Pages - *Can include sections and partials*
- Sections - *Can include only partials*
- Partials - *Can not include other components*

###Markup

####A little introduction for formatting
Please use [BEM](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/) syntax for a correct "rule declaration". For more look at [BEM 101](https://css-tricks.com/bem-101/) for peaces of advise also.
The goal of BEM is to help developers better understand the relationship between the HTML and CSS in a given project.

* Use soft tabs (2 spaces) for indentation
* Prefer dashes over camelCasing in class names.
* Underscores and PascalCasing are okay
* Do not use ID selectors
* When using multiple selectors in a rule declaration, give each selector its own line.
* Put a space before the opening brace { in rule declarations
* In properties, put a space after, but not before, the : character.
* Put closing braces } of rule declarations on a new line
* Put blank lines between rule declarations


We think it's fair to say that even though BEM won't solve all our problems it is extraordinarily useful for constructing scalable and maintainable interfaces where everyone on the team should have a clear idea of how things can be improved. This is because a great deal of front end development is not just about the nice tricks that solve one little problem in the short term; we need agreements, promises and binding social contracts between developers so that our codebase can adapt over time.

For a quick look of **TWIG templater** see [here](https://twig.symfony.com/doc/2.x/templates.html#range).

And now...

	grunt create --type=<page/section/partial> --name=<component name>


All **pages** templates **must extend** a `base... .twig` file. There is a base.twig in core:

	<!doctype html>
	<html>
		<head>
			 {% block head %}
			    <meta charset="utf-8">
			    <meta http-equiv="X-UA-Compatible" content="IE=edge">

			    <meta name="description" content="{{ site.description }}">
			    <meta name="viewport" content="width=device-width, maximum-scale=1">
			    <link rel="shortcut icon" href="@@url/images/favicon.ico" type="image/x-icon">

			    <link rel="stylesheet" href="@@url/css/custom@css_extension.css">
				{% endblock %}
		</head>
		<body>
			<div id="mobile-menu"></div>
			{% include 'components/sections/header.twig' %}
			<div class="wrapper body-wrapper">
				{% block content %}{% endblock %}
			</div> <!-- end body-wrapper -->
			{% include 'components/sections/footer.twig' %}
			<!-- Load CustomModulesLoader -->
			{% include 'components/partials/customModulesLoader.twig' %}

			<script>
			/* Load Picturefill */
			!function(a){var b=navigator.userAgent;a.HTMLPictureElement&&/ecko/.test(b)&&b.match(/rv\:(\d+)/)&&RegExp....			</script>
			<script src="@@url/js/scriptloader.js"></script>

		</body>
	</html>


This is a **page** structure example, `demo.twig`:

		<!-- demo.twig -->
		{% extends "base.twig" %}

		{% block head %}
				 <title>{{ site.name }}</title>
				{{ parent() }}
		{% endblock %}

		{% block content %}
		<main class="demo--page test">
			<div class="container">
				<div class="main-content main-content--left">
					<p>main content</p>
				</div>

				<div class="sidebar sidebar--right">
					<p>sidebar</p>
				</div>
			</div>
		</main>
		{% endblock %}
This is a **section** structure example, `guides-bl.twig`:

		<section class="guides-bl--section">
			<!-- partial title-bl -->

			<div class="container">
				<div class="col-4">
					<!-- partial guide-cl -->
				</div>

				<div class="col-4">
					<!-- partial guide-cl -->
				</div>

				<div class="col-4">
					<!-- partial guide-cs -->
					<!-- partial guide-cs -->
				</div>
			</div>
		</section>

This is a **partial** structure example, `guide-cs.twig`:

		<div class="guide-cs--partial">
			<figure data-link="...">
				<picture>
					<img src="..." alt="..." />
				</picture>
			</figure>

			<div class="container">
				<div class="container">
					<div class="span-4">
						<span class="category" data-link="...">Lorem</span>
					</div>

					<div class="span-8">
						<span class="guide-info">8 lessons - beginner</span>
					</div>
				</div>

				<a href="...">
					<h2>Lorem ipsum dolor sit amet</h2>
				</a>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Donec eget auctor sapien. Ut tristique nulla ullamcorper, sodales
				odio ut, sagittis ante.</p>
			</div>

			<!-- ... -->
		</div>


### Data params

####data-link
It adds a link and a cursor pointer to a not anchor tag element:

	<picture data-link="http://www.google.com">
		<img src="..../>
	</picture>

####data-anchor
It adds an anchor and a cursor pointer to an element in dom:

	<picture data-anchor="your id target">
		<img src="..../>
	</picture>

####data-sticky-offset-top
See below [sticky-offset-element](#sticky-offset-element)

####data-library and data-media-query
If you need a script only in a certain viewport ("mobile", large-mobile", "desktop"), you can use this:

	<div data-library="swiper" data-media-query="desktop, large-desktop">...</div>




## How to use core libraries
Rogiostrap already has libreries in core you can use.

Those are always loaded:

- jQuery
- Modernizer
- Lazysizes
- Hoverintent

Those are load conditionally:

- Swiper (.swiper-js)
- Sticky-kit (.sticky-kit-js)
- Chosen (.chosen-js)

Rogiostrap checks if some libraries is required and loads it every time the browser download the page. So to do this you have to write:

	<div class="swiper-js">...</div>


## Writing and loading your scripts

P.S.: **you have jQuery available if you don't want to write the code in vanilla js.**

You might have different initialization of an element between desktop and mobile, or mobile and large-mobile....

Write your script in function and launch that function throwgh a global variable whitch is validate every page loading, or device orientation chenge.

### This variable is launchScript, and it can be true or false.

So you can initialize, for example,  Swiper element in this way:

	function initHorizontalsSlider(){

			if(MySlider == undefined){

				if(viewport == "desktop" || viewport == "large-mobile"){

						var	 MySlider = new Swiper('.swiper-container', {

								loop: false,
								direction: 'horizontal',
								slidesPerView: 'auto',
								spaceBetween: 20,
					      grabCursor: true,
								pagination: {
									el: '.slider-horizontal-pagination',
									clickable: true,
									type: 'progressbar',
								},
								navigation: {
									nextEl: '.slider-horizontal__next',
									prevEl: '.slider-horizontal__prev',
								}
							});
				};

				if (viewport == "mobile"){

						var MySlider = new Swiper('.swiper-container', {
							loop: false,
							freeMode: true,
							direction: 'horizontal',
							slidesPerView: 'auto',
							spaceBetween: 10,
							pagination: {
								el: '.slider-horizontal-pagination',
								type: 'progressbar',
							}
						});

				}

			} else {

				MySlider( true, true );
				jQuery('.swiper-wrapper').removeAttr('style');
				jQuery('.swiper-slide').removeAttr('style');
				MySlider = undefined;
				initHorizontalsSlider()

			};

	};

	if(launchScript) initHorizontalsSlider();

###the customModulesLoader.twig file

How you can see, `base.twig` include a partial called `customModulesLoader.twig`:

	...
	{% include 'components/partials/customModulesLoader.twig' %}
	..

It allows you to loading your scripts that work your application.
Every component has its own .scss and .js files in /sass and /js folders, with the same name indeed. Eg.:

	/components/partials/menu.twig
	/js/components/patials/menu.js
	/sass/components/partials/_menu.scss

So you can include js file in customModulesLoader.twig after others objects already included, in this way:

		<script>
			window.tribooScriptModules = [
			{
				...
			},
			{
				MenuJs: {
					exists: document.getElementsByClassName('menu-js').length,
					params: {
						url: '@@components/partials/menu.js',
						key: 'menu-js',
					},
				},
			},
		];
	</script>

**Rogiostrap will load menu.js only if you insert in your HTML markup the class menu-js in an element.**

 Eg.:

	<nav class="main-menu menu-js">...</nav>



## Writing and load your styles

Once you've written your css rules you have to include the _menu.scss file in `critical.scss` if the component will appear in "above the fold", or in `custom.scss` if it will not. And that's it!
Eg.:

`critical.scss` file:

	...
	@import "components/partials/menu";


### Grid system usage
Rogiostrap use a grid system with **12 columns** (customizable) and import [Jeet! Framework](http://jeet.gs/).

**NB: on mobile viewport ALL settings get width 100%.**

The grid system basic usage it's pretty simple:

### .col-n
Elements have margin-right...

		<div class="container">
			<div class="col-10"></div>
			<div class="col-2"></div>
		</div>

### .span-n
Elements have no margin-right...

		<div class="container">
			<div class="span-10"></div>
			<div class="span-2"></div>
		</div>

### .container-cycle-col-n
This allow you to create a grid of n elements width the same width. The loop paginates children **horizontally**.

		<div class="container-cycle-col-4">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>

### .container-cycle-span-n
This allow you to create a grid of n elements width the same width. The loop paginates children **horizontally**. Set gutter via css3 rule "column-gap".


		<div class="container-cycle-span-4">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>

### .col-count-n
This allow you to create a grid of n elements width the same width. The loop paginates children **vertically**.


		<!-- something.twig -->
		<div class="col-count-n">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>

### .mobile-switch-container and .mobile-switch-n

Sets the different order of siblings element in mobile viewport.
**NB: ALL elements MUST HAVE this class defined:

		<!-- something.twig -->
		<div class="mobile-switch-container">
			<div class="mobile-switch-3">
				...
			</div>
			<div class="mobile-switch-1">
				...
			</div>
			<div class="mobile-switch-2">
				...
			</div>
		</div>

### .flex, .flex--column, .flex--center

Sets display rule to "flexbox" and its vendors, vertically centered. In mobile viewport set himself to vertical direction. Flex--column set direction to "column" in desktop viewport also, flex--center set rule "align-items" to "center":

		<div class="flex">
			<div></div>
			<div></div>
			<div></div>
		</div>

		<div class="flex flex--column flex--center">
			<div></div>
			<div></div>
			<div></div>
		</div>


### .wrapper

Is an element large "siteWith" variable (default 996px), centered in window.

### .container

Is an element that should be always present in a page component, because it sets a basic padding of 8px.

###.section-container

Use this class to a div whitch works as container of sections.


### .main-content, .main-content--left, main-content--right

Use this element into .container to define if the page has sidebar or not:

		<div class="main-content main-content--left">
			...
		</div>
		<aside class="sidebar--right>
			...
		</aside>

### .sidebar, .sidebar--left, .sidebar--right

...see above.

###.rail-800

Is an element 800px tall, to use in sidebar for containing banners or other stuff.

###.sticky-element

Class that stickable element MUST HAVE to stick in parent

###.sticky-parent

Class that reference parent for sticky element MUST HAVE.


###.sticky-offset-element

Class that sticky element parent MUST HAVE to get data-offset-sticky-top value (see [below](#Data params)...)

	<class="container sticky-parent">
		...
		<aside class="sidebar sticky-offset-element" data-sticky-offset-top="10">
			<div class="sticky-element">...</div>
			...
		</aside>
		...
	</div>



### and in the end, you can however use JEET rules...

		/* something.scss */
		.example {
			@include column(1/7, $gutter: 2);
		}

**will compile**

		/* something.css */
		.example {
			float: left;
			clear: none;
			text-align: inherit;
			width: 12.57143%;
			margin-left: 0%;
			margin-right: 2%;
		}

## Mixins

### line-clamp($lines, $line-height)

It truncates pharagraphs to passed lines in parameters

##Mediaqueries

###smartphone

(max-width: #{breakpoint}px)

###smartphone-portrait

(max-width : 320px) and (orientation: portrait)

###smartphone-landscape

(max-width: #{breakpoint}px) and (orientation: landscape)

###over-smartphone

(min-width: #{breakpoint+1}px)

###large-smartphone-portrait

(max-width: 375px) and (orientation: portrait)

###large-smartphone-landscape

(max-width: 812px) and (orientation: landscape)



## Functions and methods
Rogiostrap makes available some methods witch you can call in your code.


###viewport

It's a global variable that can be: "mobile", "large-mobile", "desktop". You can use it everywhere:

	if( viewport == "mobile" ) {...}

Variable



###rogio.log

Use rogiostrap logs for debug:

	rogio.log('my message', 'success'); //warning, failure, timer, require


You can see them in console using this query string at the end of url, otherwise they are hidden. Eg.:

	https://localhost:2323/demo.html/?eDebug


###throttle and debounce

Use as reinforcement of SetTimeout(). Eg.:

	(window).resize($.debounce(300, my-function()));
	(window).scroll($.throttle(300, my-function()));

Prototype

###hitTest

Use to check if two elements collide. Eg.:

	$('#element').hitTest(xMouse, yMouse)

Prototype, returns boolean

###Mutation

It capture mutation status of an element.

		myelement.Mutation(attribute: String, value: String, callback: Function, initialStatusValue: Boolean)
 Eg.:

	jQuery('body').Mutation('class', 'leo-skin', 'hasObserved', false);

Here we are clonig Mutation function and attached method to BODY tag and want to get if  and when class 'leo-skin' is added, then we run callback function 'hasObserved' that does some stuff in our js.

###isInViewport

Use to check if an element is fully visible in viewport. Eg.:

	$('#element').isInViewport()

Prototype, returns boolean


###blockScroll(boolean)

Use to block the scroll of viewport to avoid scroll, for example, under a modal panel.

Function 	



## Shell Commands
To begin to develop:

	grunt start

To restart developing:

	grunt server


To create a new component:

	grunt create --type=<page/section/partial> --name=<component name>

To delete an existing component:

	grunt delete --type=<page/section/partial> --name=<component name>

To use an already exist component of Rogiostrap core:

	grunt use
The framework will open a checkbox list of available components in core, from witch you can choice multiple files.
For components list you can see the Components section below.


To deploy for distribution and create .map files, preserve console logs, and minify files:

	grunt build_dev

This command builds inside the following folders:

- `/dist/css`
- `/dist/js`
- `/dist/fonts`
- `/dist/images`
- `templates`

To deploy for distribution and don't create .map files, console logs neither but minify files:

	grunt build_prod

This command builds inside the following folders:

- `/dist/css`
- `/dist/js`
- `/dist/fonts`
- `/dist/images`
- `templates`


___
#Components

There are some twig templates in Rogiostrap npm package that you can use in your project:

	grunt use

Page templates are the base for developing a Wordpress theme, and are:

##Page templates

* archive.twig
* author.twig
* base.twig
* base-modal.twig
* category.twig
* page-newsletter.twig
* single-articolo.twig
* single-video.twig

##Section templates

* 2-columns.twig
* 3-columns.twig
* footer.tig
* header.twig
* header-modal.twig
* newsletter.twig
* sidebar-home.twig
* speciale-hero-block.twig
* trend.twig

##Partial templates
___
###breadcrumbs.twig

![](https://lh4.googleusercontent.com/eyLiCSYkoq7jEieKDoquKRsqbLKkgIkb9hwYhr7uUeeRNHNaokthEC3gCRA=w2400)
___
###card-faq-medium.twig

![](https://lh3.googleusercontent.com/BlikoXa64qc7pvJ259mqD4lroYNGR28wqRxCNtkzqtNP9QvlZZ-b_fLGe34=w2400)
___
###card-news-archive.twig

![](https://lh3.googleusercontent.com/iVEel5KJSP6H1urAZLEIS1Hqi-_E__D4xJWyQPSTBmbP3S_9F7hGoiHjpUQ=w2400)
___
###card-news-big.twig

![](https://lh6.googleusercontent.com/olVvfvFpPlEuvQRfH0xbPqfyNtSEn8VnBVsfJ8RpJpZWzsMdb58HaPDAI5c=w2400)
___
###card-news-medium.twig

![](https://lh6.googleusercontent.com/AHlna0ZQnMjA-B_HbbV-ZvB2RQxpgNYYYONCNOlCYNedYcwCBFOPf6yjiJY=w2400)
___
###card-news-small.twig

![](https://lh6.googleusercontent.com/UjVUNiBp3NrC2jtY1NCh0veh4VEhW_f48tnoP7YDOWB6EYXXmFMN0Dy4MiE=w2400)
___
###card-video-archive.twig

![](https://lh3.googleusercontent.com/tox7xuhEOfFA6NLtuO4VP1XjBa7ybCKcVElvBgkDREHMNybttNGfQVbNknU=w2400)
___
###card-video-big.twig

![](https://lh3.googleusercontent.com/cgLG-KQJ3rCPU9e0ZqBfOniQn15zXHySQGjijh03XqeWAq6MSKFPYl0GHOg=w2400)
___
###customModulesLoader.twig

This component is a tag script container. It is to include your scripts for. Isn't a graphic component.
___
###forms.twig

**Macro twig component**. It group al element of a form (input, select, textarea, radio buttons, etc.)
___
###header-author.twig

![](https://lh4.googleusercontent.com/zYg2rRpCicmYPmIxevJMhBRi0T9ZbMslthm8rid-VD4KdvtZ8ICrHTTtRi4=w2400)
___
###header-menu.twig

![](https://lh4.googleusercontent.com/7jFJKqMZtBDCZjZ8Za904zDofUlu12mgQIwxL1Lxmegb0L0udIa00O3IrW4=w2400)
___
###pagination.twig

![](https://lh5.googleusercontent.com/1xYgL_EMorIz7sYsyfbUPVmGk7ABIMtrwerzFE4UW0Q9OgyI-urm0zIFNDQ=w2400)
___
###progress-bar.twig

![](https://lh6.googleusercontent.com/kKfUPUgxGqGQNMTSIdSR6_KrJJU-98zSxpqohwvz6r5dzBWFcS6anpoMzzI=w2400)
___
###related-post--esperto.twig

![](https://lh6.googleusercontent.com/6AYrwvYlYTm60VSvJNAdCpc8D_S92eQYTePaHM_A9aWJ-uJbSr-BEFDtgqM=w2400)
___
###related-post--no-image.twig

![](https://lh5.googleusercontent.com/crD8ig0bgdpJ26SUx5xJONXhsUiKo3-V0cM9Bi7EC5wubWHF-Ir0P2oA9IQ=w2400)
___
###related-post--post.twig

![](https://lh6.googleusercontent.com/o4ck8yr-Uq784egcpKH-w7jv7zF20Nt7sgsQl0FY3dmvmNiz6AMVhLDl2o4=w2400)
___
###related-post--video.twig

![](https://lh4.googleusercontent.com/7gc4JU1n_pwTem4oQM3T5HVRZxxeQsNaploPVnp5U2BjVARNWqzJSyfnpsg=w2400)
___
###search-form.twig

![](https://lh5.googleusercontent.com/uBZhgk1zTGOQO0_JDorigV_xtU-6rxWoeKwv7_UTnOp12qe8SaCJmO_ltTQ=w2400)
___
###social-white.twig

![](https://lh5.googleusercontent.com/OXMGHzDnMNnru_OEbGK_unGt-S4Os-GWdg6GUIjjvUQxjE7j7i3LAGg-0tQ=w2400)
___
###tabs.twig

![](https://lh6.googleusercontent.com/wEdEp2xIFvS1IPeJAo4o0qjwYV2fmc2sSamJSxToayvouPcI7VzeqC7MlRk=w2400)
___
###title-section.twig

**Macro component**. Section text and section link (button on the right) can be customize through parameters.

![](https://lh3.googleusercontent.com/G_TIaYMQxsH4Uw-IDnIxLD7r2t4fAKiCUZ2revX8_oqI2ISiEQs9XoE98Eo=w2400)
___
###video-player.twig

![](https://lh4.googleusercontent.com/eyeRfu7d7fKYMJNO5WNgDIiBsDWo89tbJKibz0sQtAZwbKmU_4j9hA1EIsc=w2400)

##Change log

* Documentation version: 1.0.15
* 24/10/2018 Mutation prototype added
