function skipCache() {
    var test = "test";
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return false;
    } catch (e) {
        return true;
    }
}

var basketVersion = "1565114151";

var jqueryFile = {
    url: "../node_modules/rogiostrap/bower_components/jquery/dist/jquery.min.js",
    key: "jquery",
    unique: basketVersion,
    skipCache: skipCache()
};

var rogioFiles = [ {
    url: "../node_modules/rogiostrap/bower_components/bowser/src/bowser.js",
    key: "bowser",
    unique: basketVersion,
    skipCache: skipCache()
}, {
    url: "../node_modules/rogiostrap/src/js/libs/modernizr-custom.js",
    key: "modernizr",
    unique: basketVersion,
    skipCache: skipCache()
}, {
    url: "../node_modules/rogiostrap/bower_components/lazysizes/lazysizes.min.js",
    key: "lazysizes",
    unique: basketVersion,
    skipCache: skipCache()
}, {
    url: "../node_modules/rogiostrap/src/js/libs/jquery.hoverIntent.min.js",
    key: "hoverIntent",
    unique: basketVersion,
    skipCache: skipCache()
}, {
    url: "./js/coreModulesLoader.js",
    key: "coreModules",
    unique: basketVersion,
    skipCache: skipCache()
}, {
    url: "../node_modules/rogiostrap/src/js/functions.js",
    key: "functions",
    unique: basketVersion,
    skipCache: skipCache()
} ];

if (typeof jQuery === "undefined") {
    rogioFiles.unshift(jqueryFile);
}

basket.require.apply(basket, rogioFiles);
//# sourceMappingURL=scriptloader.js.map