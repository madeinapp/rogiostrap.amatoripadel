module.exports = function (grunt) {

	require('nopt-grunt-fix')(grunt);

	var settings = grunt.file.readJSON('settings.json');



	// Configuration
	var config = {
		domain: settings.settings.domain,
		root: './',
		app: 'src',
		urlTest: '',
		urlCore: 'node_modules/rogiostrap/',
		dev: settings.settings.test,
		urlDist: settings.settings.urlBuild + '/' + settings.settings.buildFolder+ '/assets',
		urlCustomLibs: 'js/libs',
		urlComponents: 'js/components',
		dist: settings.settings.buildFolder + '/assets',
		twigDist: settings.settings.buildFolder + '/views'
	};

	grunt.initConfig({
		config: config,
		//pkg: grunt.file.readJSON('package.json'),

		// Http-server
		// https://github.com/area/grunt-http-server
		// Twig render
		// https://github.com/stefanullinger/grunt-twig-render
		twigRender: {
			dev: {
				files: [
					{
						data: 'data.json',
						expand: true,
						cwd: '<%= config.app %>/',
						src: [
							'**/*.twig',
						],
						dest: '<%= config.root %>/<%= config.dev %>',
						ext: '.html',
					},
				],
			},
			prod: {
				files: [
					// {
					// 	data: 'data.json',
					// 	expand: true,
					// 	cwd: '<%= config.app %>/components/partials/',
					// 	src: [
					// 		'customModulesLoader.twig',
					// 	],
					// 	dest: './templates',
					// 	ext: '.html'
					// },
					{
						data: 'data.json',
						expand: true,
						cwd: '<%= config.app %>/',
						src: [
							'**/*.twig',
						],
						dest: './templates',
						ext: '.html',
					},
				],
			},
		},

		// Clean
		// https://github.com/gruntjs/grunt-contrib-clean
		clean: {
			options: {
				force: true,
			},
			dev_html: {
				src: [
					'<%= config.dev %>/*.html',
				],
			},
			dev_images: {
				src: [
					'<%= config.dev %>/images/',
				],
			},
			dev_fonts: {
				src: [
					'<%= config.dev %>/fonts/',
				],
			},
			prod: {
				src: [
					settings.settings.buildFolder
				],
			},
		},

		// Copy
		// https://github.com/gruntjs/grunt-contrib-copy
		copy: {
			dev: {
				files: [
					{
						flatten: true,
						expand: true,
						cwd: '<%= config.app %>',
						src: [
							'sass/**/*.css',
						],
						dest: '<%= config.dev %>/css/',
					}
				]
			},
			build: {
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>',
						src: [
							'**/*.twig',
						],
						dest: '<%= config.twigDist %>/',
					},
					{
						flatten: true,
						expand: true,
						cwd: '<%= config.app %>',
						src: [
							'sass/**/*.css',
						],
						dest: '<%= config.dist %>/css/',
						ext: '.min.css'
					},
					{
						expand: true,
						cwd: '<%= config.app %>',
						src: [
							'fonts/**/*',
						],
						dest: '<%= config.dist %>',
					},
					{
						expand: true,
						cwd: '<%= config.urlCore %>src',
						src: [
							'images/*',
						],
						dest: '<%= config.dist %>',
					},
					{
						expand: true,
						flatten: true,
						cwd: '<%= config.urlCore %>bower_components',
						src: [
							'jquery/dist/jquery.min.js',
							'bowser/src/bowser.js',
							'swiper/dist/js/swiper.min.js',
							'sticky-kit/jquery.sticky-kit.min.js',
							'lazysizes/lazysizes.min.js',
							'chosen/chosen.jquery.min.js',
						],
						dest: '<%= config.dist %>/js/libs',
					},
					{
						expand: true,
						cwd: 'bower_components',
						src: [

						],
						dest: '<%= config.dist %>/js/libs',
					},
				],
			},
		},


		'http-server': {
			dev: {
				root: './',
				port: 2323,
				host: 'localhost',
				showDir: true,
				autoIndex: true,
				ext: 'html',
				runInBackground: true,
				openBrowser: true,
				logFn: function (req, res, error) {},
			},
		},

		// Watch
		// https://github.com/gruntjs/grunt-contrib-watch
		watch: {
			options: {
				livereload: true,
			},
			twig: {
				files: [
					'<%= config.app %>/**/*.twig',
				],
				tasks: [
					'clean:dev_html',
					'twigRender:dev',
					'string-replace:dev_HTML',
				],
			},
			dataJson: {
				files: [
					'<%= config.app %>/*.json',
				],
				tasks: [
					'clean:dev_html',
					'twigRender:dev',
					'string-replace:dev_HTML',
					'sync'
				],
			},
			html: {
				files: [
					'<%= config.app %>/**/*.html',
				],
				tasks: [
					'clean:dev_html',
					'string-replace:dev_HTML',
				],
			},
			php: {
				files: [
					'<%= config.app %>/*.php',
				],
				tasks: [
					'sync',
				],
			},
			sass: {
				files: [
					'<%= config.app %>/sass/**/*.scss',
				],
				tasks: [
					'sass:dev',
					'postcss:dev',
				],
			},
			css: {
				files: [
					'<%= config.app %>/sass/**/*.css',
				],
				tasks: [
					'copy:dev',
				],
			},
			js: {
				files: [
					'<%= config.app %>/js/**/*',
				],
				tasks: [
					'uglify:dev',
					'string-replace:dev_JS',
				],
			},
			img: {
				files: [
					'<%= config.app %>/images/**/*',
				],
				tasks: [
					'clean:dev_images',
					'imagemin:dev',
				],
			},
			fonts: {
				files: [
					'<%= config.app %>/fonts/**/*',
				],
				tasks: [
					'clean:dev_fonts',
					'sync',

				],
			},
		},

		// Sync
		// https://github.com/tomusdrw/grunt-sync
		sync: {
			main: {
				files: [
					{
						cwd: '<%= config.app %>',
						src: [
							'*.html',
							'*.htm',
							'*.php',
							//'css/**/*.css',
							'fonts/**',
							'images/*',
							'*.json'
						],
						failOnError: true,
						dest: '<%= config.dev %>',
					},
				],
				verbose: true,
				updateAndDelete: false,
				//ignoreInDest: '<%= config.dev %>/js/scriptloader.js',
			},
		},

		// Sass
		// https://github.com/gruntjs/grunt-contrib-sass
		sass: {
			dev: {
				//includePaths: ['<%= config.urlCore %>' + 'src/sass/'],
				options: {
					sourcemap: 'none',
					style: 'expanded',
					compass: true,
					trace: true,
					quiet: false,
					lineNumbers: true
				},
				files:
					[{
		        expand: true,
		        cwd: '<%= config.app %>/sass/',
		        src: ['*.scss','!_*.scss','components/partials/*.scss','components/sections/*.scss'],
		        dest: '<%= config.dev %>/css/',
		        ext: '.css'
		      }]
			},
			build_dev: {
				options: {
					sourcemap: 'none',
					style: 'compact',
					compass: true,
					trace: true,
					quiet: false,
					lineNumbers: true,
				},
				files:
					[{
						expand: true,
						cwd: '<%= config.app %>/sass/',
						src: ['*.scss','!_*.scss','components/partials/*.scss','components/sections/*.scss'],
						dest: '<%= config.dist %>/css/',
						ext: '.css'
					}]
			},
			build_prod: {
				options: {
					sourcemap: 'none',
					style: 'compact',
					compass: true,
					trace: false,
					quiet: true,
					lineNumbers: true,
				},
				files:
					[{
						expand: true,
						cwd: '<%= config.app %>/sass/',
						src: ['custom.scss','components/partials/*.scss','components/sections/*.scss'],
						dest: '<%= config.app %>/css/',
						ext: '.css'
					}]
			},
			build_file: {
				options: {
					sourcemap: 'none',
					style: 'compact',
					compass: true,
					trace: false,
					quiet: true,
					lineNumbers: true,
				},
				files: {
					'<%= config.dev %>/css/<%= templateCss %>.css': '<%= config.app %>/sass/<%= templateCss %>.scss',
					'<%= config.dist %>/css/<%= templateCss %>.min.css': '<%= config.app %>/sass/<%= templateCss %>.scss',
				},
			},
		},

		// Postcss
		// https://github.com/nDmitry/grunt-postcss
		postcss: {
			dev: {
				options: {
					map: {
						inline: false,
						annotation: '<%= config.dev %>/css/maps/',
					},

					processors: [
						require('pixrem')(),
						require('autoprefixer')({
							browsers: 'last 2 versions',
						}),
					],
				},
				expand: true,
				cwd: '<%= config.dev %>/css/',
				src: '**/*.css',
				dest: '<%= config.dev %>/css/',
				ext: '.css',
			},
			build_dev: {
				options: {
					map: {
						inline: false,
						annotation: '<%= config.dist %>/css/maps/',
					},
					failOnError: true,
					processors: [
						require('pixrem')(),
						require('autoprefixer')({
							browsers: 'last 2 versions',
						}),
					],
				},
				expand: true,
				cwd: '<%= config.app %>/css/',
				src: '**/*.css',
				dest: '<%= config.dist %>/css/',
				ext: '.min.css',
			},
			build_prod: {
				options: {
					map: {
						inline: false,
						annotation: '<%= config.dist %>/css/maps/',
					},
					failOnError: true,
					processors: [
						require('pixrem')(),
						require('autoprefixer')({
							browsers: 'last 2 versions',
						}),
						require('cssnano')(),
					],
				},
				expand: true,
				cwd: '<%= config.app %>/css/',
				src: '**/*.css',
				dest: '<%= config.dist %>/css/',
				ext: '.min.css',
			},
		},

		// Imagemin
		// https://github.com/gruntjs/grunt-contrib-imagemin
		imagemin: {
			dev: {
				options: {
					optimizationLevel: 5,
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>/images',
						src: ['**/*.{png,jpg,gif,svg,ico}'],
						dest: '<%= config.dev %>/images/',
					},
				],
			},
			prod: {
				options: {
					optimizationLevel: 5,
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>/images',
						src: ['**/*.{png,jpg,gif,svg,ico}'],
						dest: '<%= config.dist %>/images/',
					},
				],
			},
		},

		// Uglify
		// https://github.com/gruntjs/grunt-contrib-uglify
		uglify: {
			dev: {
				options: {
					compress: false,
					beautify: true,
					sourceMap: true,
					mangle: false,
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/',
						src: [
							'*.js',
							'!main.js',
							'!functions.js',
						],
						dest: '<%= config.dev %>/js/',
					},
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/libs/',
						src: '*.js',
						dest: '<%= config.dev %>/js/libs/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/',
						src: '**/*.js',
						dest: '<%= config.dev %>/js/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/components/',
						src: '**/*.js',
						dest: '<%= config.dev %>/js/components/',
					},
				],
			},
			build_dev: {
				options: {
					compress:false,
					mangle:false,
					beautify: true,
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/',
						src: [
							'*.js',
						],
						dest: '<%= config.dist %>/js/',
					},
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/libs/',
						src: '*.js',
						dest: '<%= config.dist %>/js/libs/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/components/',
						src: '**/*.js',
						dest: '<%= config.dist %>/js/components/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/',
						src: '**/*.js',
						dest: '<%= config.dist %>/js/',
					},
				],
			},
			build_prod: {
				options: {
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true,
					},
					mangle: false,
					beautify: true,
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/',
						src: [
							'*.js',
						],
						dest: '<%= config.dist %>/js/',
					},
					{
						expand: true,
						cwd: '<%= config.urlCore %>src/js/libs/',
						src: '*.js',
						dest: '<%= config.dist %>/js/libs/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/components/',
						src: '**/*.js',
						dest: '<%= config.dist %>/js/components/',
					},
					{
						expand: true,
						cwd: '<%= config.app %>/js/',
						src: '**/*.js',
						dest: '<%= config.dist %>/js/',
					},
				],
			},
		},



		// String-replace
		// https://github.com/eruizdechavez/grunt-string-replace
		'string-replace': {
			dev_HTML: {
				files: [
					{
						expand: true,
						cwd: '<%= config.dev %>',
						dest: '<%= config.dev %>',
						src: ['**/*.html'],
					},
				],
				options: {
					replacements: [
						{
							pattern: /@css_extension/g,
							replacement: '',
						}, {
							pattern: /@@urlmain/g,
							replacement: '../<%= config.urlCore %>src/js',
						}, {
							pattern: /@@url\//g,
							replacement: '',
						}, {
							pattern: /@@components/g,
							replacement: '<%= config.urlComponents %>',
						}, {
							pattern: /@@version/g,
							replacement: '<%= Math.floor((Date.now() / 1000)) %>',
						}, {
							pattern: /@@csrf_field/g,
							replacement: '',
						}, {
							pattern: /@@publicStorage/g,
							replacement: 'images/',
						},
					],
				},
			},
			dev_JS: {
				files: [
					{
						expand: true,
						cwd: '<%= config.dev %>/js',
						dest: '<%= config.dev %>/js',
						src: [
							'**/*.js',
						],
					},
				],
				options: {
					replacements: [
						{
							pattern: /@@urlSame/g,
							replacement: './js',
						},
						{
							pattern: /@@urlmain/g,
							replacement: '../<%= config.urlCore %>src/js',
						}, {
							pattern: /@@url/g,
							replacement: '../<%= config.urlCore %>bower_components',
						}, {
							pattern: /@@customLibs/g,
							replacement: '../<%= config.urlCore %>src/<%= config.urlCustomLibs %>',
						}, {
							pattern: /@@components/g,
							replacement: '../<%= config.urlCore %>src/<%= config.urlComponents %>',
						}, {
							pattern: /@@version/g,
							replacement: '<%= Math.floor((Date.now() / 1000)) %>',
						},
					],
				},
			},
				build_dev_Twig: {
					files: [
						{
							expand: true,
							cwd: '<%= config.twigDist %>',
							dest: '<%= config.twigDist %>',
							src: [
								'**/*.twig',
							],
						},
					],
					options: {
						replacements: [
							{
								pattern: /@css_extension/g,
								replacement: '',
							},{
								pattern: /@@urlmain/g,
								replacement: '<%= config.urlDist %>/js',
							},{
								pattern: /@@components/g,
								replacement: '<%= config.urlDist %>/<%= config.urlComponents %>',
							},{
								pattern: /@@url/g,
								replacement: '<%= config.urlDist %>',
							},{
								pattern: /@@csrf_field/g,
								replacement: '{{ csrf_field() }}',
							},{
								pattern: /@@publicStorage/g,
								replacement: '/public/storage/',
							},
						],
					},
				},
			build_dev_HTML: {
				// files: [
				// 	{
				// 		expand: true,
				// 		cwd: './templates/twig/components/partials/',
				// 		dest: './templates',
				// 		src: [
				// 			'customModulesLoader.twig',
				// 		],
				// 	},
				// ],
				files: [
					{
						expand: true,
						cwd: './templates',
						dest: './templates',
						src: [
							'*.html',
						],
					},
				],
				options: {
					replacements: [
						{
							pattern: /@css_extension/g,
							replacement: '',
						},{
							pattern: /@@urlmain/g,
							replacement: '<%= config.urlDist %>/js',
						},{
							pattern: /@@components/g,
							replacement: '<%= config.urlDist %>/<%= config.urlComponents %>',
						},{
							pattern: /@@url/g,
							replacement: '<%= config.urlDist %>',
						},
					],
				},
			},
			build_prod_HTML: {
				// files: [
				// 	{
				// 		expand: true,
				// 		cwd: './templates/twig/components/partials/',
				// 		dest: './templates',
				// 		src: [
				// 			'customModulesLoader.twig',
				// 		],
				// 	},
				// ],
				files: [
					{
						expand: true,
						cwd: './templates',
						dest: './templates',
						src: [
							'*.html',
						],
					},
				],
				options: {
					replacements: [
						{
							pattern: /@css_extension/g,
							replacement: '.min',
						},{
							pattern: /@@urlmain/g,
							replacement: '<%= config.urlDist %>/js',
						},{
							pattern: /@@components/g,
							replacement: '<%= config.urlDist %>/<%= config.urlComponents %>',
						},{
							pattern: /@@url/g,
							replacement: '<%= config.urlDist %>',
						},
					],
				},
			},
			prod_JS: {
				files: [
					{
						expand: true,
						cwd: '<%= config.dist %>/js',
						dest: '<%= config.dist %>/js',
						src: [
							'scriptloader.js',
							'coreModulesLoader.js',
						],
					},
				],
				options: {
					replacements: [
						{
							pattern: /@@urlSame/g,
							replacement: '<%= config.urlDist %>/js',
						},
						{
							pattern: /@@urlmain/g,
							replacement: '<%= config.urlDist %>/js',
						}, {
							pattern: /@@url\/[^@]+\//g,
							replacement: '<%= config.urlDist %>/<%= config.urlCustomLibs %>/',
						}, {
							pattern: /@@customLibs/g,
							replacement: '<%= config.urlDist %>/<%= config.urlCustomLibs %>',
						}, {
							pattern: /@@components/g,
							replacement: '<%= config.urlDist %>/<%= config.urlComponents %>',
						}, {
							pattern: /@@version/g,
							replacement: '<%= Math.floor((Date.now() / 1000)) %>',
						},
					],
				},
			},
		},

		// Http
		// https://github.com/johngeorgewright/grunt-http
		http: {
			download: {
				options: {
					url: 'http://admin:888@<%= templateUrl %>',
					timeout: 7000,
				},
				dest: '<%= config.app %>/temporary-template/<%= downloaded %>.html',
			},
		},

		notify_hooks: {
				options: {
						enabled: true,
						success: true,
						duration: 3
				}
		},

		// Bump
		bump: {
				options: {
						files: ['package.json'],
						updateConfigs: [],
						commit: true,
						commitMessage: 'Release v%VERSION%',
						commitFiles: ['package.json'],
						createTag: true,
						tagName: '%VERSION%',
						tagMessage: 'Version %VERSION%',
						push: true,
						pushTo: 'origin',
						gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
						globalReplace: false,
						prereleaseName: false,
						regExp: false
				}
		},

		// Create
		create: {
			checkFiles: {
				flatten: true,
				cwd: '<%= config.app %>/',
				src: [
					'**/*.twig',
				],
				nonull: true,
			},
		},

		// Delete
		delete: {
			checkFiles: {
				flatten: true,
				cwd: '<%= config.app %>/',
				src: [
					'**/*.twig',
				],
				nonull: true,
			},
		},

		prompt: {
	    type: {
	      options: {
	        questions: [
					{
	          config:  'componentType',
	          type:    'list',
	          message: 'What kind of component do you want to create?',
						default: 'partial',
	          choices: [
	            {
	              value: 'section',
	              name:  'Section'
	            },
	            {
	              value: 'partial',
	              name:  'Partial'
	            }
	          ]
	      	}
					],
					then: function(answers){
						var list = grunt.file.expand({filter: "isFile", cwd: "node_modules/rogiostrap/src/components/" + answers.componentType + 's'},['*.twig']);
						grunt.config.set('componentsList', list);
						grunt.task.run(['prompt:component']);

					}
	      }
	    },
			component: {
				options: {
					questions: [
					{
						config:   'components',
						type:     'checkbox',
						message:  'Chose component from the core:',
						choices:  '<%= componentsList %>'
					}
					],
					then: function(selectedComponents){
						var type = grunt.config('componentType');
						var components = selectedComponents.components;
						var nodeUrl = "node_modules/rogiostrap/src/";
						components.forEach(function(item,index){
							var fileName = item.slice(0, -5);
							grunt.file.copy( nodeUrl + "components/" + type + "s/" + item, "src/components/" + type + "s/" + item );
							if (grunt.file.exists(nodeUrl + "js/components/" + type + "s/" + fileName + ".js")){
								grunt.file.copy( nodeUrl + "js/components/" + type + "s/" + fileName + ".js", "src/js/components/" + type + "s/" + fileName + ".js" );
							}else {
								grunt.file.write("src/js/components/" + type + "s/" + fileName + ".js", "/* script for " + fileName +  ".twig */");
							}
							grunt.file.copy( nodeUrl + "sass/components/" + type + "s/" + fileName + ".scss", "src/sass/components/" + type + "s/" + fileName + ".scss" );
						});
						grunt.log.writeln('\n \u2713 Components: ' + components.toString() + ' created.');
					}
				}
			},

	  }

	});

	// Tasks
	grunt.loadNpmTasks('grunt-bump');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-http-server');
	grunt.loadNpmTasks('grunt-sync');
	grunt.loadNpmTasks('grunt-string-replace');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-twig-render');
	grunt.loadNpmTasks('grunt-prompt');

	// Task -> rogiostrap start
	grunt.registerTask('start', function () {
		//var pkg = grunt.file.readJSON('package.json');
		grunt.file.mkdir(config.dev + '/css/components/sections');
		grunt.file.mkdir(config.dev + '/css/components/partials');
		grunt.task.run([
			'twigRender:dev',
			'sync',
			'sass:dev',
			'postcss:dev',
			'copy:dev',
			'string-replace:dev_HTML',
			'uglify:dev',
			'string-replace:dev_JS',
			'imagemin:dev',
			'http-server:dev',
			'watch',
		]);
	});


	grunt.registerTask('fast', function () {  // without images
		grunt.file.mkdir(config.dev + '/css/components/sections');
		grunt.file.mkdir(config.dev + '/css/components/partials');
		grunt.task.run([
			'twigRender:dev',
			'sync',
			'sass:dev',
			'postcss:dev',
			'copy:dev',
			'string-replace:dev_HTML',
			'uglify:dev',
			'string-replace:dev_JS',
			'http-server:dev',
			'watch',
		]);
	});

	// Task -> rogiostrap server
	grunt.registerTask(
		'server', [
			'http-server:dev',
			'watch',
		]
	);

	// Task -> rogiostrap build_dev
	grunt.registerTask(
		'build_dev', [
			'clean:prod',
			'twigRender:prod',
			'copy:build',
			'sass:build_dev',
			//'string-replace:build_dev_Twig',
			'string-replace:build_dev_HTML',
			'postcss:build_dev',
			'uglify:build_dev',
			'string-replace:prod_JS',
			'imagemin:prod',
		]
	);

	// Task -> rogiostrap build_prod
	grunt.registerTask(
		'build_prod', [
			'clean:prod',
			//'twigRender:prod',
			'copy:build',
			'sass:build_prod',
			'string-replace:prod_HTML',
			'postcss:build_prod',
			'uglify:build_prod',
			'string-replace:prod_JS',
			'imagemin:prod',
		]
	);


	//grunt css --name=file.scss
	grunt.registerTask('css', function () {

		var file = grunt.option('file') || 'custom';
		grunt.config.set('templateCss', file);

		grunt.task.run([
			'sass:build_file',
			'sass:build_dev',
			'postcss:build_dev'
		]);
	});

	// Task create component
	// grunt create --type=<page, section, partial> --name=<name>
	grunt.registerMultiTask('create', 'detect if files exist', function () {
		var myPath = '';
		var type = grunt.option('type');
		var name = grunt.option('name') || 'component';
		var myFile =  name + '.twig';

		if (type === 'page') {
			myPath = '' + name + '.twig';
		} else if (type === 'section') {
			myPath = 'components/sections/' + name + '.twig';
		} else if (type === 'partial') {
			myPath = 'components/partials/' + name + '.twig';
		} else {
			grunt.fail.fatal('\n \u2717 Type non valido o non definito.');
		};

		var filechecked = 0;
		this.files.forEach(function (f) {
			f.src.map(function (filepath) {
				var fileName = filepath.substr(filepath.lastIndexOf('/') + 1);
				if (fileName == name + '.twig') {
					console.log('\n \u2717 Component già esistente, utilizza un altro nome.');
					return false;
				};

				filechecked++;
				if (filechecked >= f.src.length) {
					createComponent(myPath);
				}
			});
		});

		function createComponent(myPath) {

				if (type === 'page') {
					grunt.file.write('src/' + myPath, '{% extends "base.twig" %}\n\n{% block head %}\n<title>{{ site.name}} - </title>\n{{ parent() }}\n{% endblock %}\n\n{% block content %}\n\n<!--' + name + '.twig ' + type + ' -->\n\n<div class="' + name + '">\n\n</div>\n{% endblock %}');
					grunt.file.write('src/sass/' + name + '.scss', '/* ' + name +  '.twig styles */\n\n.' + name + ' {\n\n}');
					grunt.task.run(['sass:dev','postcss:dev']);
				} else if (type === 'section') {
					grunt.file.write('src/' + myPath, '<!-- ' + name + '.twig ' + type + ' -->\n\n<div class="' + name + '">\n\n{% if loop.index <= 1 and '+ name +' is not defined or loop.index is not defined and ' + name + ' is not defined %}\n\n<link rel="stylesheet" href="@@url/css/components/'+ type +'s/'+ name +'@css_extension.css" />\n\n{% endif %}\n\n<!-- code here -->\n\n</div>');
					grunt.file.write('src/sass/components/sections/' + name + '.scss', '/* ' + name + '.twig styles */\n\n@import "customVariables";\n\n.' + name + ' {\n\n}');
					grunt.file.write('src/js/components/sections/' + name + '.js', '/* ' + name +  '.twig scripts */');
					grunt.task.run(['sass:dev','postcss:dev']);
				} else if (type === 'partial') {
					grunt.file.write('src/' + myPath, '<!-- ' + name + '.twig ' + type + ' -->\n\n<div class="' + name + '">\n\n{% if loop.index <= 1 and '+ name +' is not defined or loop.index is not defined and ' + name + ' is not defined %}\n\n<link rel="stylesheet" href="@@url/css/components/'+ type +'s/'+ name +'@css_extension.css" />\n\n{% endif %}\n\n<!-- code here -->\n\n</div>');
					grunt.file.write('src/sass/components/partials/' + name + '.scss', '/* ' + name +  '.twig styles */\n\n@import "customVariables";\n\n.' + name  + ' {\n\n}');
					grunt.file.write('src/js/components/partials/' + name + '.js', '/* ' + name +  '.twig scripts */');
					grunt.task.run(['sass:dev','postcss:dev']);
				}

				grunt.log.writeln('\n \u2713 Component ' + type + ' "' + name + '.twig" creato.');
		};
	});

	// Task delete component
	// grunt delete --type=<page, section, partial> --name=<name>
	grunt.registerMultiTask('delete', 'delete file if exists', function () {
		var myPath;
		var type = grunt.option('type');
		var name = grunt.option('name');
		var myFile =  name + '.twig';

		if (type === 'page') {
			myPath = '' + name + '.twig';
		} else if (type === 'section') {
			myPath = 'components/sections/' + name + '.twig';
		} else if (type === 'partial') {
			myPath = 'components/partials/' + name + '.twig';
		};

		var filechecked = 0;
		var fileFound = 0;
		this.files.forEach(function (f) {
			f.src.map(function (filepath) {
				var fileName = filepath.substr(filepath.lastIndexOf('/') + 1);
				if (fileName == name + '.twig') {
					deleteComponent(myPath);
					fileFound++;
				};

				filechecked++;
				if (filechecked >= f.src.length && fileFound == 0) {
					console.log('\n \u2717 Nessun component di nome "' + name + '.twig" è stato trovato');
				}
			});
		});

		function deleteComponent(myPath) {
			if (type != undefined) {
				grunt.file.delete('src/' + myPath);
				if (type === 'page') {
					grunt.file.delete('src/sass/' + name + '.scss');
					grunt.file.delete('local/css/' + name + '.css');
				} else if (type === 'section') {
					grunt.file.delete('src/sass/components/sections/' + name + '.scss');
					grunt.file.delete('local/css/components/sections/' + name + '.css');
					grunt.file.delete('src/js/components/sections/' + name + '.js');
				} else if (type === 'partial') {
					grunt.file.delete('src/sass/components/partials/' + name + '.scss');
					grunt.file.delete('local/css/components/partials/' + name + '.css');
					grunt.file.delete('src/js/components/partials/' + name + '.js');
				}
			} else {
				grunt.fail.fatal('\n \u2717 Type non valido.');
			}

			grunt.log.writeln('\n \u2713 Component ' + type + ' "' + name + '.twig" eliminato.');
		}
	});

	// Task use component

	grunt.registerTask('use',function(){
		grunt.task.run('prompt:type');
	});

};
